﻿using UnityEngine;
using System.Collections;

public class ShootPattern : MonoBehaviour {
    public int scatterCount;
    public float funnelAngle;
    public float rotationSpeed;
    public float fireDelay;
    public float beginShootDelay;
    public float shootDirectionAngle;

    public GameObject bulletObject;
    public Transform weaponPos;

    private float fireCounter;
    private float angle_step;
    private float start_ang;
    private Vector3 fireDirection;
    private bool readyToShoot;
    void Start()
    {
        start_ang = shootDirectionAngle - (funnelAngle / 2.0f);
        angle_step = funnelAngle / (float)(scatterCount - 1);
        readyToShoot = false;
        StartCoroutine(delayShoot());
    }

	// Update is called once per frame
	void Update () {
        fireCounter += Time.deltaTime;
        start_ang += rotationSpeed * Time.deltaTime;
        if (fireCounter >= fireDelay && readyToShoot)
        {
            float aim_angle = start_ang;
            for (int i = 0; i < scatterCount; i++)
            {
                weaponPos.rotation = Quaternion.Euler(0.0f, 0.0f, aim_angle);
                Instantiate(bulletObject, weaponPos.position, weaponPos.rotation);
                aim_angle += angle_step;
            }
            fireCounter = 0.0f;
        }
	}

    public void fireNow()
    {
        fireCounter = fireDelay;
    }

    IEnumerator delayShoot()
    {
        yield return new WaitForSeconds(beginShootDelay);
        readyToShoot = true;
    }
}
