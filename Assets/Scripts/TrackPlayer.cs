﻿using UnityEngine;
using System.Collections;

public class TrackPlayer : MonoBehaviour {
    public InGameManager gameManager;
    public float trackingFactor;
    public float activationDelay;

    private Transform playerTransform;
    private float velocity;
    private Vector3 oldVector;
    private Vector3 currentVector;
    private Vector3 targetVector;
    private float trackerT;
	// Use this for initialization
	void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InGameManager>() ;
        playerTransform = gameManager.getPlayerTransform();
        velocity = GetComponent<Rigidbody>().velocity.magnitude;
        oldVector = currentVector = targetVector = Vector3.left;//playerTransform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(trackPlayerPosition());
	}

    IEnumerator trackPlayerPosition()
    {
        yield return new WaitForSeconds(activationDelay);
        trackerT += trackingFactor * Time.deltaTime;

        if (trackerT > 1.0f)
        {
            targetVector = (playerTransform.position - transform.position);
            oldVector = currentVector;
            trackerT = 0.0f;
        }
        currentVector = Vector3.Slerp(oldVector, targetVector, trackerT);
        //transform.rotation = Quaternion.FromToRotation(Vector3.right, -currentVector);
        transform.rotation = Quaternion.FromToRotation(Vector3.right, -currentVector);
        GetComponent<Rigidbody>().velocity = velocity * currentVector.normalized;
        
    }
}
