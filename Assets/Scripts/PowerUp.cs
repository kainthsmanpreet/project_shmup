﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {
	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().velocity = 5.0f * Vector3.left;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider otherObj)
    {
        if (otherObj.tag != "Boundary")
        {
            otherObj.gameObject.GetComponent<Weapon>().onWeaponChange(GetComponent<Weapon>());
            Destroy(this.gameObject);
        }
    }
}
