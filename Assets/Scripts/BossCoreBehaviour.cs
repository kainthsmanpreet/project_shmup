﻿using UnityEngine;
using System.Collections;

public class BossCoreBehaviour : MonoBehaviour {
    public GameObject innerShield;
    public GameObject outerShield;

    public MonoBehaviour homingBlade;
    public MonoBehaviour trackBeam1;
    public MonoBehaviour trackBeam2;
    public MonoBehaviour spreadFire1;
    public MonoBehaviour straightBeam1;

    public float state1Limit;
    public float state2Limit;
    public float state3Limit;

    private float timeInState;
    private Animator thisAnimator;
    private Animator innerAnimator, outerAnimator;
    private AnimatorStateInfo coreLayer1Info;
    private AnimatorStateInfo coreLayer2Info;

    StateMachine stateMachine;

	// Use this for initialization
	void Start () {
        stateMachine = new StateMachine(entryState);
        timeInState = 0.0f;

        innerShield.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;
        outerShield.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity;

        thisAnimator = GetComponent<Animator>();
        innerAnimator = innerShield.GetComponent<Animator>();
        outerAnimator = outerShield.GetComponent<Animator>();    
        
	}

	// Update is called once per frame
	void Update () {
        timeInState += Time.deltaTime;
        stateMachine.Update();
	}

    void slowRot()
    {
        if (timeInState > state1Limit)
        {
            stateMachine.setState(fastRot);
            timeInState = 0.0f;
            homingBlade.enabled = false;
            trackBeam1.enabled = false;
            trackBeam2.enabled = false;
            spreadFire1.enabled = true;
        }
        innerAnimator.speed = 0.3f;
        outerAnimator.speed = 0.35f;
        thisAnimator.speed = 0.4f;
        
    }

    void fastRot()
    {
        if (timeInState > state2Limit)
        {
            stateMachine.setState(noRot);
            timeInState = 0.0f;
            spreadFire1.enabled = false ;
            straightBeam1.enabled = true;
            innerAnimator.SetBool("loop", false);
            outerAnimator.SetBool("loop", false);
        }
        innerAnimator.speed = 1.2f;
        outerAnimator.speed = 1.45f;
        thisAnimator.speed = 0.8f;
    }

    void noRot()
    {
        if (timeInState > state3Limit)
        {
            stateMachine.setState(slowRot);
            timeInState = 0.0f;
            homingBlade.enabled = true;
            trackBeam1.enabled = true ;
            trackBeam2.enabled = true;
            straightBeam1.enabled = false;
            innerAnimator.SetBool("loop", true);
            outerAnimator.SetBool("loop", true);
        }

        thisAnimator.speed = 0.4f;

    }

    void entryState()
    {
        if (transform.position.x <= 8.35f)
        {
            stateMachine.setState(slowRot);
            timeInState = 0.0f;
            GetComponent<SphereCollider>().enabled = true;
            thisAnimator.enabled = true;
            innerAnimator.enabled = true;
            outerAnimator.enabled = true;
            homingBlade.enabled = true;
            trackBeam1.enabled = true;
            trackBeam2.enabled = true;
        }
    }
}
