﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public GameObject backgroundQuad;
    public int numBackgrounds;
    public float scaleWidth;
    public float zValue;
    //static int backgroundCount = 3;
    // Use this for initialization
	void Start () {
        for (int i = 0; i < numBackgrounds; i++)
        {
            Vector3 position = new Vector3(i * scaleWidth, 0.0f, zValue);
            Quaternion rotation = Quaternion.identity;
            Instantiate(backgroundQuad, position, rotation);

        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
