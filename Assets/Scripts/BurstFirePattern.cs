﻿using UnityEngine;
using System.Collections;

public class BurstFirePattern : MonoBehaviour {
    public int burstScatterCount;
    public float burstFunnelAngle;
    public float burstFireDelay;
    
    public int scatterCount;
    public float funnelAngle;
    public float rotationSpeed;
    public float fireDelay;
    public float beginShootDelay;
    public float shootDirectionAngle;
    public bool isTracking;
    public float overrideVel;

    public GameObject bulletObject;
    public Transform weaponPos;

    private float fireCounter;
    private float angle_step;
    private float start_ang;
    private Quaternion shootDirection;
    private InGameManager gameManager;

    void trackPlayer()
    {
        Transform playerTransform = gameManager.getPlayerTransform();
        weaponPos.transform.rotation = Quaternion.FromToRotation(Vector3.right, playerTransform.position - weaponPos.transform.position);
        playerTransform.position = new Vector3(playerTransform.position.x,
                                                playerTransform.position.y,
                                                transform.position.z);
        transform.rotation = Quaternion.FromToRotation(Vector3.right, playerTransform.position - transform.position);
        //Debug.Log(playerTransform.position - transform.position);
        //Debug.Log(transform.rotation);
    }

    void Start()
    {
        start_ang = shootDirectionAngle - (funnelAngle / 2.0f);
        angle_step = funnelAngle / ((scatterCount > 1)?(scatterCount - 1):1);
        fireCounter = 0.0f;
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        fireCounter += Time.deltaTime;
        start_ang += rotationSpeed * Time.deltaTime;

        if (isTracking)
        {
            trackPlayer();
            shootDirection = transform.rotation;//Quaternion.FromToRotation(Vector3.right, weaponPos.position - transform.position) ;
            shootDirectionAngle = shootDirection.eulerAngles.z;
            start_ang = /*shootDirectionAngle*/ - (funnelAngle / 2.0f);
        }

        if (fireCounter >= fireDelay)
        {
            float aim_angle = start_ang;
            for (int i = 0; i < scatterCount; i++)
            {
                //aim_angle = shootDirectionAngle - (funnelAngle / 2.0f);
                float burst_angle = aim_angle - (burstFunnelAngle / 2.0f);
                StartCoroutine(burstFire(burst_angle));
                //StartCoroutine(burstFire(shootDirection));
                aim_angle += angle_step;
            }
            fireCounter = 0.0f;
        }
    }

    IEnumerator burstFire(float direction)
    {
        float bAngle = direction;//.eulerAngles.z;
        for (int j = 0; j < burstScatterCount; j++)
        {
            //Quaternion temprot = Quaternion.Euler(0.0f, 0.0f, weaponPos.rotation.eulerAngles.z + bAngle) ;
            Quaternion temprot = Quaternion.Euler(0.0f, 0.0f, bAngle) * weaponPos.rotation;
            GameObject gObj = Instantiate(bulletObject, weaponPos.position, temprot) as GameObject;

            if (overrideVel != 0.0f)
                gObj.GetComponent<Rigidbody>().velocity = overrideVel * (temprot * Vector3.right);

            bAngle += burstFunnelAngle / (burstScatterCount - 1);
            yield return new WaitForSeconds(burstFireDelay);
        }
    }


}
