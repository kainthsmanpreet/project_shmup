﻿using UnityEngine;
using System.Collections;

public class ScrollTexture : MonoBehaviour {

    public float scrollSpeed;
    public Renderer renderer;
    private float offset;
	// Use this for initialization
	void Start () {
        renderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        offset += Time.deltaTime * scrollSpeed;
        renderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
	}
}
