﻿using UnityEngine;
using System.Collections;

public class TransformCamera : MonoBehaviour {

    public float speed;
	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().velocity = speed * Vector3.right;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
