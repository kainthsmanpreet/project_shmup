﻿using UnityEngine;
using System.Collections;

public class InputControl : MonoBehaviour {
    public Transform weaponPosition;
    public float movementSpeed;

    private Vector3 parentVelocity;
    private Weapon weapon;
    private bool isRunning;
    private GameObject pauseMenuObj;
    private float menuInputTime;
    private float menuInputCounter;
    private Rigidbody playerRigidBody;
    private Quaternion playerOriginalQuaternion;
    private float t, deltaT;
    //private float maxRotationOfShip;

    public float minX, maxX;
    public float minY, maxY;

	// Use this for initialization
	void Start () {
        //GetComponentInParent<Rigidbody>().velocity = transform.parent.gameObject.GetComponent<Rigidbody>().velocity;
        //parentVelocity = transform.parent.gameObject.GetComponent<Rigidbody>().velocity;
        weapon = GetComponent<Weapon>();
        isRunning = true;
        menuInputTime = 1f;
        menuInputCounter = 0.0f;
        playerRigidBody = GetComponent<Rigidbody>();
        playerOriginalQuaternion = transform.rotation;
        t = 0.0f;
        deltaT = 1.1f;
	}
	
	// Update is called once per frame
	void Update () {
        processInput();
        clampPlayerPosition();
	}

    void FixedUpdate() {
        //float vMove = Input.GetAxis("Vertical");
        //float hMove = Input.GetAxis("Horizontal");

        //Vector3 vel = new Vector3(hMove, vMove, 0.0f);
        //GetComponent<Rigidbody>().velocity = movementSpeed * vel; 
    }

    void processInput()
    {
        Vector3 movementVector = Vector3.zero;
        Quaternion playerRotation = playerOriginalQuaternion;
        if (isRunning)
        {           
            if (Input.GetKey(KeyCode.LeftControl))
            {
                weapon.Fire(weaponPosition.position, weaponPosition.rotation);
            }

            else
                weapon.resetCoolDown();

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                movementVector.x = -movementSpeed;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                movementVector.x = movementSpeed;
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                movementVector.y = movementSpeed;
                playerRotation = Quaternion.AngleAxis(30.0f, Vector3.right);
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                movementVector.y = -movementSpeed;
                playerRotation = Quaternion.AngleAxis(-30.0f, Vector3.right);               
            }

            if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                t = 0.0f;
            }
                

        }
        //t += deltaT * Time.deltaTime;
        //t = Mathf.Clamp01(t);
        //Debug.Log(t);
        playerRigidBody.velocity = movementVector;
        transform.rotation = playerRotation;
    }

    void clampPlayerPosition()
    {
        transform.position = new Vector3
            (Mathf.Clamp(transform.position.x, minX, maxX),
            Mathf.Clamp(transform.position.y, minY, maxY),
            0.0f           
            );
    }

    public void toggleGameRunning(bool runState)
    {
        isRunning = runState;
    }

}
