﻿using UnityEngine;
using System.Collections;

public enum WeaponType { RAPID_FIRE, SPREAD_SHOT, SEEKER};

public class Weapon : MonoBehaviour
{
    public float shootDelay;
    public int bulletCount;
    public GameObject projectile;
    public float vSpacing;
    public float funnelAngle;
    public WeaponType currentWeaponType;

    private float fireCounter;
    private float angleCounter;
    private float spacingCounter;
    private float spacingStep;
    private float angleStep;
    // Use this for initialization
    void Start()
    {
        angleStep = funnelAngle / (bulletCount - 1);
        spacingStep = 0.0f;
        currentWeaponType = WeaponType.RAPID_FIRE;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void onWeaponChange(Weapon someWeapon)
    {
        this.shootDelay = someWeapon.shootDelay;
        this.funnelAngle = someWeapon.funnelAngle;
        this.bulletCount = someWeapon.bulletCount;
        this.projectile = someWeapon.projectile;
        angleStep = (bulletCount > 1) ? (funnelAngle / (bulletCount - 1)) : 0;
    }
    
    public void Fire(Vector3 startPos, Quaternion startRot)
    {
        fireCounter += Time.deltaTime;
        if (fireCounter >= shootDelay)
        {
            spacingCounter = -1.0f;
            angleCounter = -funnelAngle / 2.0f;
            for (int i = 0; i < bulletCount; i++)
            {
                Vector3 pos = startPos;
                pos.y += spacingCounter * spacingStep;
                Quaternion rotation = Quaternion.AngleAxis(angleCounter, Vector3.forward);
                Instantiate(projectile, pos, rotation);
                angleCounter += angleStep;  
                spacingCounter++;
            }
            fireCounter = 0.0f;
        }

    }

    public void resetCoolDown()
    {
        fireCounter = shootDelay;
    }
}