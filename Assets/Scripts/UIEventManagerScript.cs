﻿using UnityEngine;
using System.Collections;

public class UIEventManagerScript : MonoBehaviour {

    private InGameManager gameMgr;
	// Use this for initialization
	void Start () {
        GameObject gameControllerObj = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObj)
            gameMgr = gameControllerObj.GetComponent<InGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartGame()
    {
        Application.LoadLevel("scene_1");
    }

    public void ReturnToMainMenu()
    {
        Application.LoadLevel("Menu");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Resume()
    {
        gameMgr.pauseTheGame(gameMgr.pausedMenu);
    }

    public void tutOK()
    {
        gameObject.SetActive(false);
        //gameMgr.pauseTheGame();
    }
}
