﻿using UnityEngine;
using System.Collections;

public class EnemySineBehaviour : MonoBehaviour {
    public float timerStep;
    public float initialPhase;
    public float amplitude;

    private Vector3 basePosition;
    private float timer;

    private Vector3 vel;
	// Use this for initialization
	void Start () {
        timer = 0.0f;
        basePosition = transform.position;
        vel = gameObject.GetComponent<Rigidbody>().velocity;
	}
	
	// Update is called once per frame
	void Update () {
        timer += timerStep * Time.deltaTime;
        enemyBehaviour();
	}

    void enemyBehaviour()
    {
        Vector3 temp = basePosition;
        temp.y = basePosition.y + (2.0f * Mathf.Sin(timer + Mathf.Deg2Rad * initialPhase));
        temp.x += vel.x * timer;
        //Debug.Log(basePosition.y + (40.0f * Mathf.Sin(timer)));
        transform.position = temp;
    }
}
