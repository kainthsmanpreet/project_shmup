﻿using UnityEngine;
using System.Collections;


public class MoveAlongPath : MonoBehaviour {
    public float duration;
    public BezierSpline bzSpline;
    
    private float t;
    public MonoBehaviour behaviourComponent;

	// Use this for initialization
	void Start () {
        t = 0.0f;
        //behaviourComponent = GetComponent<EnemyLeeBehaviour>();
        if (behaviourComponent)
            GetComponent<Animator>().speed = 2.0f / duration;
        //bzSpline = GetComponent<BezierSpline>();
	}
	
	// Update is called once per frame
	void Update () {
        if (t <= 1.0f && bzSpline != null)
        {
            t += Time.deltaTime / duration;

            //transform.localPosition = somePos;
            transform.position = bzSpline.GetPoint(t);
            //transform.LookAt(transform.position + bzSpline.GetDirection(t));

        }

        else if (behaviourComponent)
        {
            behaviourComponent.enabled = true;
            this.enabled = false;
        }
	}
}
