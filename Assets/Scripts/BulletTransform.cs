﻿using UnityEngine;
using System.Collections;

public class BulletTransform : MonoBehaviour {

    public float bulletSpeed;
    public bool isRotating;
    
	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().velocity = (transform.rotation * (Vector3.right) * bulletSpeed);
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, transform.rotation.eulerAngles.z);
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
