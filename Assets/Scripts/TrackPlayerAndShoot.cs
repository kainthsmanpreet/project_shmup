﻿using UnityEngine;
using System.Collections;

public class TrackPlayerAndShoot : MonoBehaviour {
    public Transform weaponPos;
    public Transform trackPlayerTransform;
    public GameObject bulletObject;
    public float trackingFactor;
    public float fireDelay;
    public float beginShootDelay;

    private float fireCounter;
    private Vector3 aimVector;
    private InGameManager gameMgr;
    private bool isActive;

	// Use this for initialization
	void Start () {
        aimVector = Vector3.right;
        gameMgr = GameObject.FindGameObjectWithTag("GameController").GetComponent<InGameManager>();
        trackPlayerTransform = gameMgr.getPlayerTransform();
        fireCounter = -999.0f;
        StartCoroutine(sleepOnBegin());
	}
	
	// Update is called once per frame
	void Update () {
        trackPlayerAndFire();
	}

    void trackPlayerAndFire()
    {
        fireCounter += Time.deltaTime;
        if (fireCounter >= fireDelay)
        {
            Vector3 newVector = trackPlayerTransform.position - weaponPos.position;
            aimVector = aimVector + ((trackingFactor) * (newVector - aimVector));
            aimVector.z = 0.0f;
            weaponPos.rotation = Quaternion.FromToRotation(Vector3.right, aimVector.normalized);
            Instantiate(bulletObject, weaponPos.position, weaponPos.rotation);
            fireCounter = 0.0f;
        }
    }

    IEnumerator sleepOnBegin()
    {
        yield return new WaitForSeconds(beginShootDelay);
        fireCounter = fireDelay;
    }
}
