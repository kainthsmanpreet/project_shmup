﻿using UnityEngine;
using System.Collections;

public class AsteroidBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * Random.Range(0.7f, 5.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
