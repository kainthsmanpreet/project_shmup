﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnCollideBehaviour : MonoBehaviour {

    public string affectedByTag;
    public List<Renderer> affectedMeshes;

    private Animator thisAnimator;
    private List<Color> thisColor;

	// Use this for initialization
	void Start () {
        thisAnimator = GetComponent<Animator>();
        thisColor = new List<Color>();
        for (int i = 0; i < affectedMeshes.Count; i++)
        {
            thisColor.Add(affectedMeshes[i].material.color);
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider otherObj)
    {
        if (otherObj.tag == affectedByTag)
        {
            //thisAnimator.SetBool("isHit", true) ;
            StartCoroutine(animatorCoroutine());
        }
    }

    IEnumerator animatorCoroutine()
    {
        //thisAnimator.SetBool("isHit", true);
        //yield return null;
        //thisAnimator.SetBool("isHit", false);

        for (int i = 0; i < affectedMeshes.Count; i++)
            affectedMeshes[i].material.color = Color.white;
        
        yield return new WaitForSeconds(0.06f);

        for (int i = 0; i < affectedMeshes.Count; i++)
            affectedMeshes[i].material.color = thisColor[i];
    }
}
