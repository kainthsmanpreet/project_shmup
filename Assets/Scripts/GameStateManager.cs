﻿using UnityEngine;
using System.Collections;

public enum GameState { MAIN_MENU, IN_GAME};

public delegate void OnStateChangeHandler();

public class GameStateManager : Object
{
    private GameStateManager() { }
    public static GameStateManager gsm_Instance = null;
    public event OnStateChangeHandler OnStateChange;
    public GameState gameState { get; private set; }

    public static GameStateManager gsmInstance
    {
        get
        {
            if (GameStateManager.gsm_Instance == null)
            {
                DontDestroyOnLoad(GameStateManager.gsm_Instance);
                gsm_Instance = new GameStateManager();
            }
            return GameStateManager.gsm_Instance;
        }
    }

    public void SetGameState(GameState gameState)
    {
        this.gameState = gameState;
        OnStateChange();
    }

    public void OnApplicationQuit()
    {
        GameStateManager.gsm_Instance = null;
    }

}
