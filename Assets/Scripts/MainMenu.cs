﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
    GameStateManager gsMgr;
	
	void Awake () {
        gsMgr = GameStateManager.gsmInstance;
        gsMgr.OnStateChange += OnStateChangedFn;
        Debug.Log("Main menu awoken, gamestate = " +gsMgr.gameState);
    }

    void Start()
    {
        Debug.Log("Main menu started, gamestate = " + gsMgr.gameState);
    }


	// Update is called once per frame
	void Update () {
	
	}

    public void OnStateChangedFn()
    {

    }

    public void LoadLevel()
    {
        Application.LoadLevel("Menu");
    }
}
