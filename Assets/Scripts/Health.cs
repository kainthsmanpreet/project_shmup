﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
    public int hitPoints;
    public GameObject explosionObject;
    public int scoreContribution;

    private InGameManager gameMgrObject;

    void Start()
    {
        gameMgrObject = GameObject.FindGameObjectWithTag("GameController").GetComponent<InGameManager>();
        if(gameMgrObject == null)
            Debug.Log("Not found");
    }

    void Update()
    {
        checkHealth();
    }

    void checkHealth()
    {
        if (hitPoints <= 0)
        {
            Instantiate(explosionObject, this.gameObject.transform.position, Quaternion.identity);

            if (transform.gameObject.tag == "Player")
            {
                gameMgrObject.killPlayer();
            }
            else
            {
                Destroy(this.gameObject);
                gameMgrObject.updatePlayerScore(scoreContribution);
            }
        }
    }

    public void gotHit(int dmg)
    {
        hitPoints -= dmg;
    }
}
