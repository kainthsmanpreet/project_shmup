﻿using UnityEngine;
using System.Collections;

public class RemoveOutOfBound : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerExit(Collider obj)
    {
        if (obj.tag == "Background")
        {
            Vector3 bgPosition = obj.gameObject.transform.position;
            bgPosition.x += 3.0f * 36.0f;
            obj.gameObject.transform.position = bgPosition;
            return;
        }
        Destroy(obj.gameObject);
    }
}
