﻿using UnityEngine;
using System.Collections;

public class EnemyBulletTransform : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().velocity = GetComponent<Transform>().rotation * (Vector3.right) * 20.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
