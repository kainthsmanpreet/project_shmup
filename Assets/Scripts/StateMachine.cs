﻿using UnityEngine;
using System.Collections;
using System;

public class StateMachine
{
    public delegate void ActiveState();
    public ActiveState activeState;
    // Use this for initialization

    public StateMachine(Action state)
    {
        activeState = new ActiveState(state);
    }

    // Update is called once per frame
    public void Update()
    {
        activeState();
    }

    public void setState(Action newState)
    {
        activeState = new ActiveState(newState);
    }
}
