﻿using UnityEngine;
using System.Collections;

public class DestroyOnBulletHit : MonoBehaviour {
    public int dmgDealt;
    public bool destroySelf;
    public GameObject explosionObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider obj)
    {
        if (obj.tag != "Boundary")
        {
            Health healthComponent = obj.gameObject.GetComponent<Health>();
            //Debug.Log("Hit obj named: " + obj.gameObject.ToString());
            //Debug.Log("Dmg dealt: " + dmgDealt);
            //Debug.Log("Health: " + healthComponent.hitPoints);
            if (obj.tag != "PlayerBullet")
                if(healthComponent)
                    healthComponent.gotHit(dmgDealt);

           //Debug.Log("After Health: " + healthComponent.hitPoints);

            if (destroySelf)
            {
                if(explosionObject)
                    Instantiate(explosionObject, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
