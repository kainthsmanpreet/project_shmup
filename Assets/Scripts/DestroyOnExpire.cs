﻿using UnityEngine;
using System.Collections;

public class DestroyOnExpire : MonoBehaviour
{
    public float duration;
    void Start()
    {
        Destroy(this.gameObject, duration);
    }
}
