﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InGameManager : MonoBehaviour {
    public GameObject pausedMenu;
    public GameObject gameOverMenu;
    public GameObject highScoreNotification;
    public GameObject highScoreNotificationCounter;
    public GameObject playerObject;
    public Text scoreText;
    public Text livesText;
    public Text highScoreText;
    public Vector3 playerSpawnPosition;
    public GameObject playerShield;

    private int playerScore;
    private int prevHighScore;
    private int playerLives;
    private bool isRunning;
    private InputControl playerControl;
    private bool gameOver;
    private float scoreDisplayDelta;
    private Text highScoreNotificationText;
	// Use this for initialization
	void Start () {
        isRunning = true;
        Time.timeScale = (isRunning ? 1.0f : 0.0f);
        playerScore = 0;
        prevHighScore = PlayerPrefs.GetInt("HighScore");
        playerLives = 3;
        
        livesText.text = playerLives.ToString();
        highScoreText.text = prevHighScore.ToString();
        playerControl = playerObject.GetComponent<InputControl>();
        StartCoroutine(spawnPlayer());
        gameOver = false;
        PlayerPrefs.SetInt("HighScore", 0);
        highScoreNotificationText = highScoreNotificationCounter.GetComponent<Text>();
        //pauseTheGame();
	}
	
	// Update is called once per frame
	void Update () {
        processInput();
	}

    void processInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !gameOver)
        {
            pauseTheGame(pausedMenu);
        }
    }

    public void pauseTheGame(GameObject pauseGameObject)
    {
        isRunning = !isRunning;
        pauseGameObject.SetActive(!isRunning);
        playerControl.toggleGameRunning(isRunning);
        Time.timeScale = (isRunning ? 1.0f : 0.0f);
    }

    IEnumerator prepareRespawn()
    {
        updatePlayerLives(-1);
        if (playerLives < 0)
        {
            yield return new WaitForSeconds(1.0f);
            gameOver = true;
            if (prevHighScore < playerScore)
            {
                float tempScore = 0.0f;
                highScoreNotification.SetActive(true);
                scoreDisplayDelta = playerScore * 0.05f;
                Debug.Log(scoreDisplayDelta);
                while(tempScore <= playerScore) {
                    highScoreNotificationText.text = tempScore.ToString();
                    tempScore += scoreDisplayDelta;
                    yield return null;
                }

                PlayerPrefs.SetInt("HighScore", playerScore);
                yield return new WaitForSeconds(2.0f);
                highScoreNotification.SetActive(false);
            }
            pauseTheGame(gameOverMenu);
        }
        else
        {
            yield return new WaitForSeconds(1.0f);
            StartCoroutine(spawnPlayer());
        }
    }

    public IEnumerator spawnPlayer()
    {
        playerObject.transform.position = playerSpawnPosition;
        playerObject.GetComponent<Health>().hitPoints = 50;
        playerObject.SetActive(true);
        playerObject.GetComponent<Health>().hitPoints = 65000;
        playerShield.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        playerObject.GetComponent<Health>().hitPoints = 50;
        playerShield.SetActive(false);
    }



    public void updatePlayerScore(int val)
    {
        playerScore += val;        
        scoreText.text = playerScore.ToString();
    }

    public void updatePlayerLives(int val)
    {
        playerLives += val;
        if(playerLives >= 0)
            livesText.text = playerLives.ToString();
    }

    public void killPlayer()
    {
        playerObject.SetActive(false);
        StartCoroutine(prepareRespawn());
    }

    public Transform getPlayerTransform()
    {
        return playerObject.transform;
    }
}
