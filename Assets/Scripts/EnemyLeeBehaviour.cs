﻿using UnityEngine;
using System.Collections;

public class EnemyLeeBehaviour : MonoBehaviour {
    private InGameManager gameMgr;
    private Transform playerTransform;
    private Transform mainTransform;
    private float timer;
    private ShootPattern enemyWeapon;
    private float translateSpeed;
    private Animator thisAnimator;

	// Use this for initialization
	void Start () {
        gameMgr = GameObject.FindGameObjectWithTag("GameController").GetComponent<InGameManager>();
        playerTransform = gameMgr.getPlayerTransform();
        mainTransform = transform.parent;
        enemyWeapon = mainTransform.gameObject.GetComponent<ShootPattern>();
        thisAnimator = mainTransform.gameObject.GetComponent<Animator>();
        timer = 0.0f;
        translateSpeed = 8.0f;
        StartCoroutine(EnemyBehaviour());
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    IEnumerator EnemyBehaviour()
    {
        thisAnimator.speed = 0.0f;
        // *Time.deltaTime;
        while (true)
        {
            timer = 0.0f;
            Vector3 targetPos = mainTransform.position;
            Vector3 oldPos = mainTransform.position;
            targetPos.y = playerTransform.position.y;
            float offset = targetPos.y - oldPos.y;
            thisAnimator.speed = (Mathf.Abs(offset) > 4.0f) ? (translateSpeed / offset) : 0.0f;

            while (timer <= 1.0f)
            {
                mainTransform.position = Vector3.Lerp(oldPos, targetPos, timer);
                timer += (translateSpeed / Mathf.Abs(offset)) * Time.deltaTime;
                yield return new WaitForSeconds(0.0f);
            }
            thisAnimator.speed = 0.0f;
            //yield return new WaitForSeconds(1.0f);
            enemyWeapon.fireNow();
            yield return new WaitForSeconds(1.1f);
        }
    }

    //IEnumerator moveTowardsPlayerY()
    //{
        
    //}
}
