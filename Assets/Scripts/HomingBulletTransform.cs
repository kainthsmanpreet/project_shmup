﻿using UnityEngine;
using System.Collections;

public class HomingBulletTransform : MonoBehaviour
{
    public float bulletSpeed;
    public bool isRotating;
    public float homingFactor;
    public string targetTag;
    public float activationDelay;
    public int rotateBullet;

    private Transform targetPosition;
    private GameObject[] gameObjectArr;
    private Vector3 bulletVelocity;
    private Vector3 targetVel;
    private float t;
    private int prevObjId;
    private int gameObjIndex;
    // Use this for initialization
    void Start()
    {
        bulletVelocity = bulletSpeed * (transform.rotation * (Vector3.right + new Vector3(0.0f, Random.Range(-0.1f, 0.1f), 0.0f)));
        GetComponent<Rigidbody>().velocity = bulletVelocity;
        targetVel = bulletVelocity;
        transform.rotation = Quaternion.FromToRotation(Vector3.right, bulletVelocity.normalized);
        t = -999.0f;
        //gameObjectArr = GameObject.FindGameObjectsWithTag(targetTag);
        //if (gameObjectArr.Length > 0)
        //{
        //    gameObjIndex = Random.Range(0, gameObjectArr.Length);
        //    targetPosition = gameObjectArr[gameObjIndex].transform;
        //    targetVel = (targetPosition.position - transform.position).normalized;
        //    prevObjId = gameObjectArr[gameObjIndex].GetInstanceID();
        //}

        StartCoroutine(sleepBeforeActivate());
    }


    // Update is called once per frame
    void Update()
    {   
        gameObjectArr = GameObject.FindGameObjectsWithTag(targetTag);
        if (gameObjectArr.Length > 0)
        {
            if (gameObjIndex >= gameObjectArr.Length || targetPosition == null)
            {    
                gameObjIndex = Random.Range(0, gameObjectArr.Length);
                targetPosition = gameObjectArr[gameObjIndex].transform;
            }

            if (t >= 1.0f ) 
            {
                t = 0.0f;
                targetVel = (targetPosition.position - transform.position).normalized;
                bulletVelocity = GetComponent<Rigidbody>().velocity;
            }

            GetComponent<Rigidbody>().velocity = bulletSpeed * Vector3.Slerp(bulletVelocity.normalized, targetVel.normalized, t);
            transform.rotation = Quaternion.FromToRotation(Vector3.right, rotateBullet * GetComponent<Rigidbody>().velocity);
            t += homingFactor * Time.deltaTime;
        }
    }

    IEnumerator sleepBeforeActivate()
    {
        yield return new WaitForSeconds(activationDelay);
        t = 0.0f;
    }
}