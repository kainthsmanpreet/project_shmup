﻿using UnityEngine;
using System.Collections;

public class SpawnObjects : MonoBehaviour {
    public int numberOfSpawns;
    public GameObject spawnObject;
    public float spawnVelocity;
    public Vector3 spawnerVelocity;
    public Transform spawnOffset;
    public float spawnDelay;
    public Transform playerTransform;
    public BezierSpline bSpline;
    public float spawnPhaseAngle;
    public bool isTracking;
    public float duration;
    
    private Rigidbody rgdBody;
    private InGameManager gameMgr;
    private Vector3 actualSpawnPos;
	// Use this for initialization
	void Start () {
        rgdBody = GetComponent<Rigidbody>();
        rgdBody.velocity = spawnerVelocity;
        gameMgr = GameObject.FindGameObjectWithTag("GameController").GetComponent<InGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    IEnumerator beginSpawn()
    {   
        for (int i = 0; i < numberOfSpawns; i++)
        {
            if (spawnOffset)
                actualSpawnPos = spawnOffset.position;
            else
                actualSpawnPos = transform.position;

            GameObject gameObj = Instantiate(spawnObject, actualSpawnPos, transform.rotation) as GameObject;
            gameObj.GetComponent<Rigidbody>().velocity = new Vector3(spawnVelocity, 0.0f, 0.0f);

            if (isTracking)
            {
                //gameObj.GetComponent<TrackPlayerAndShoot>().trackPlayerTransform = playerTransform;
                gameObj.GetComponent<TrackPlayerAndShoot>().trackPlayerTransform = gameMgr.getPlayerTransform();

            }
            if(bSpline) {
                gameObj.GetComponent<MoveAlongPath>().bzSpline = bSpline;
                gameObj.GetComponent<MoveAlongPath>().duration = this.duration;
            }
            if (spawnPhaseAngle != 0.0f)
                gameObj.GetComponent<EnemySineBehaviour>().initialPhase = spawnPhaseAngle;
            
            yield return new WaitForSeconds(spawnDelay);
        }
        yield return new WaitForSeconds(duration);
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.tag == "Boundary") {
            rgdBody.velocity = new Vector3(0.0f, 0.0f, 0.0f);
            StartCoroutine(beginSpawn());
        } 
    }
}
